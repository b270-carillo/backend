let http = require("http");

const port = 4000
//1
const server = http.createServer((request, response) => {
	// HTTP method of incoming request can be accessed via "method" property in request parameter
	// "GET" means that we will be retrieving/reading information 
	if ( request.url == "/items" && request.method == "GET") {
		// Rest the "/items" path and "GETS" information
		response.writeHead(200, { "Content-Type": "text/plain"});
		// Ends the request-response cycle
		response.end("Data retrieved from the database")
	}
//2
	if (request.url == "/items" && request.method == "POST") {
		response.writeHead (200, {'Content-Type': 'text/plain'})
		response.end('Data to be sent to the database')
	}
});

server.listen(port);
console.log(`Server is live at port ${port}`)