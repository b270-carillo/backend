// alert("Hello,Batch 270!");

//This is a statement.

console.log("Hello again!");
console.log("Hi, Batch 270!");

/*
	- There are two types of comments
				1. Single-line denoted by two slashes.
				2. Multi-line denoted by a slash and asterisk
*/

// [Section] Variable
	/*
		-It is used to contain data
	*/

// Declaring Variables
// Syntax: let/const variableName;
/*
	Trying to print out a value of a variable that has not been declared will return an error of undefined
	Variable must be declared first with value before they  can be used.
*/
	let myVariable;
	console.log(myVariable);

	let greeting = "Hello";
	console.log(greeting);

// Declaring and initializing variable
// Initializing variables - the instance when a variable is given its initial value
// Syntax: let/const variableName; value;
// let keyword is used if we want to reaasign values to our variable
let productName = 'desktop computer';
productName = "Laptop"
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
//interest = 2.5;
console.log(interest)

let friend = "Kate";
console.log(friend);

// we cannot re-declare variables with its scope
let friend1 = "Jay";
console.log(friend1)

// [Section] Data Types

// String - series of characters that creat a word a phrase a sentence or anything  related to  creating a text
// Strings can be written using either a single or double quote.
let country = "Philippines";
let province = "Batangas";
console.log(country);
console.log(province);

// concatenating strings
// combined to create single string using + symbol
let fulAddress = province + ", " + country;
console.log(fulAddress);

console.log("I live in the " + country);

let mailAddress = 'Metro Manila\Philippines';
console.log(mailAddress)

let message = "John\'s employess went home early.";
console.log(message)

//Numbers
	// Integers/Whole Numbers
	let headCount = "26";
	console.log(headCount);

	//Decimal Numbers
	let grade = 98.7;
	console.log(grade);

	// exponential notation
	let planetDistance = 2e10;
	console.log(planetDistance);

// Boolean - values is either true or false

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays - are special kind of data type  that's used to store multiple
// Arrays  can store diffent data types but is normally used to store data types
// Syntax: let/const arryName = [elementsA,elementB,]
let grades = [98.7, 92.1, 90.2, 95.6];
console.log(grades);

let details = ["Jogn", "Smith", 32, true];
console.log(details);

// Objects
/*
	Syntax:
	let.const objectName = {
	property: ValuesA,
	property: ValueB
	}

*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "09873636366"],
	address: {
		houseNumber: "345" ,
		city: "Manila"
	}
};
console.log(person);


//Null

let spouse = null;
console.log(spouse);

