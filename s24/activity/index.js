//console.log("k 2023")

//1
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

//2
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

//3
const animal = {
  name: "Lolong",
  kind: "saltwater crocodile",
  weight: "1075 kgs",
  measurement: "20 ft 3 in",
};
const { name, kind, weight, measurement } = animal;
console.log(
  `${name} was a ${kind}. He weighed at ${weight} with a measurement ${measurement}.`
);

//4
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

//5
let reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);




//6
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);
