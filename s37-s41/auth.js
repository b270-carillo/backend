const jwt = require("jsonwebtoken");

// User defined string data that will be used to create our JSON web token
// Used in the algorithm for encrypting our date which make difficult to decode the information without the defined secret code
const secret = "CourseBookingAPI";

// [Section] JSON Web Tokens
/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code can decode the encrypted information

- Imagine JWT as a gift wrapping service that secures the gift with a lock
- Only the person who knows the secret code can open the lock
- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
- This ensures that the data is secure from the sender to the receiver
*/

// Token creation
// Analogy: Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data,secret, {});
}


module.exports.verify = (req, res, next) => {
	
	let token = req.headers.authorization;

	if(token !== undefined) {
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"});

			} else {

				next();
			}
		})
	} else {
		return res.send({auth: "failed"});
	}
}


module.exports.decode = (token) => {

	if (token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null;
			} else {

				return jwt.decode(token, {complete:true}).payload;
			}
		})
	} else {
		return null;
	}
}
