const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth.js")


// Route for creating a course
// router.post("/", courseController.addCourse); Refactor

router.post("/", auth.verify, courseController.addCourse);


router.get("/all", auth.verify, courseController.getAllCourses);


router.get("/", courseController.getAllActive);


router.get("/:courseId", courseController.getCourse);


router.put("/:courseId", auth.verify, courseController.updateCourse);


// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, courseController.archiveCourse)



module.exports = router;

