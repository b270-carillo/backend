// console.log("Happy Wednesday!");

// Array Methods
// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items

// [SECTION] Mutator Methods
/*
    - These are methods that "mutate" or change an array after they are created.
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
    - Adds an element at the end of an array AND returns the new array's length
    - Syntax:
        arrayName.push(elementToBeAdded);
*/

console.log("Current fruits array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); //5 - array's length
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
/*
    - Removes the last element in an array AND returns the removed element
    - Syntax:
        arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit); //Guava

console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift()
/*
    - Adds one or more elements at the beginning of an array AND returns the new array
    - Syntax:
    arrayName.unshift(elementA, elementB);
*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

// shift()
/*
    - Removes an element at the beginning of an array and returns the removed element
    - Syntax:
        arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit); //Lime
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
    - Simultaneously removes elements from a specified index number and adds elements.
    - Returns the removed element/s
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

let fruitSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitSplice); //Apple and Orange - removed elements
console.log("Mutated array from splice method: ");
console.log(fruits);

//sort


fruits.sort()
console.log("Mutated array from sort method: ");
console.log(fruits);

//reverse()
fruits.reverse()
console.log("Mutated array from reverse method: ");
console.log(fruits);

//Non-mutator methods
/*
	-non-mutator methods are m
*/



let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE",];
console.log(countries);

//indexOf

let firstIndex = countries.indexOf("PH", 2);
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);


//last index()

let lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndex);


//slice

let sliceArrayA = countries.slice(2);
console.log("Result of slice method:");
console.log(sliceArrayA);



let sliceArrayB = countries.slice(2, 5);
console.log("Result of slice method:");
console.log(sliceArrayB);


let sliceArrayC = countries.slice(-3);
console.log("Result of slice method:");
console.log(sliceArrayC);

//toSting

let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);


//concat()

let taskArrayA = ["drink html", "eat javacript"];
let taskArrayB = ["inhale css", "breathe bootstrap"];
let taskArrayc = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);


//Combining arrays
console.log("Result from concat method: ");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayc);
console.log(allTasks);


let combinedTasks = taskArrayA.concat("smell express", "throw react");

console.log("Result from concat method: ");
console.log(combinedTasks);

//join

let users = ["John", "Jane", "Joe", "Joshua"]
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

//[SECTION] iteration methods

//forEach()

let filteredTasks = [];

allTasks.forEach(function(task) {

	if(task.length > 10) {
		filteredTasks.push(task);
	}
})

console.log("Result of filtered tasks: ");
console.log(filteredTasks);


// map()

let numbers = [1, 2, 3, 4,5];

let numberMap = numbers.map(function(number){
	return number * number;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method");
console.log(numberMap);


///map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
})
console.log(numberForEach);

// every()

let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log("Result of every method: ");
console.log(allValid);

// some()

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result of some method: ");
console.log(someValid);


// filter()

let filterValid = numbers.filter(function(number){
	return (number < 3 );
});
console.log("Result of filter method: ");
console.log(filterValid);

// includes()

let products = ["mouse", "keyboard", "laptop", "monitor"];

let productFound1 = products.includes("mouse");
console.log(productFound1);

let productFound2 = products.includes("headset");
console.log(productFound2);


//reduce()

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

let list = [ "hello", "again", "world"];

let reduceString = list.reduce(function(x, y) {
	return x + " " + y;
})

console.log("Result of reduce method: " + reduceString);