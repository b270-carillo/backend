//console.log("pogi")

//1

let number = Number(prompt("Give me a number."))
function printNumbers(number) {
  console.log("The number you provided is " + number);
  for (let count = number; count >= 0; count--) {
    if (count === 50) {
      console.log(
        "The current value is at " + count + ". Terminating the loop."
      );
      break;
    } else if (count % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
    } else if (count % 5 === 0) {
      console.log(count);
    }
  }
}

printNumbers(number);




//2

let string = "supercalifragilisticexpialidocious";
console.log(string);
let filteredString = "";

for (let i = 0; i < string.length; i++) {
  if (
    string[i] == "a" ||
    string[i] == "e" ||
    string[i] == "i" ||
    string[i] == "o" ||
    string[i] == "u"
  ) {
    continue;
  } else {
    filteredString += string[i];
  }
}
console.log(filteredString);


