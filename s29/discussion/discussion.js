// Comparison Query Operator


// === [SUB-SECTION] $gt/gte Operator
	
	// Allows us to find documents that have field number values greater than or equal to a specific value
	/*
		Syntax:
			db.collectionName.find({field: {$gt: value} });
			db.collectionName.find({field: {$gte: value} });
	*/

db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 50 } });


//  ==== [SUB-SECTION] $lt/$lte Operator

	//  Allow us to find documents that have field number values less than or equal to a specific value
	/*
		Syntax:
			db.collectionName.find({field: {$lt: value} });
			db.collectionName.find({field: {$lte: value} });
	*/

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });



//  ==== [SUB-SECTION] $ne Operator
	
	// Allows us to find document that have field number values not equal to a specified value
	/*
		Syntax:
			db.collectionName.find({ field: {$ne: value} })
	*/


db.users.find({ age: { $ne: 82 } });



//  ==== [SUB-SECTION] $in Operator

	// Allows us to find documents with specific match criteria one field using different values
	/*
		Syntax:
			db.collectionName.find({ field: {$in: value} })
	*/

db.users.find({ lastName: { $in: [ "Hawking", "Doe" ] } });
db.users.find({ courses: { $in: [ "HTML", "React" ] } });


//  ==== [SECTION] Logical Query Operator

// == [SUB-SECTION] $or Operator

	// Allows us to find documents that match a single criteria from multiple provided search criteria
	/*
		Syntax:
			db.collectionName.find({ $or: [ { fieldA: valueA }, { fieldA: valueB } ]})
	*/

db.users.find({ $or: [ { firstName: "Neil" }, { age: 25 } ] });
db.users.find({ $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] });

//  == [SUB-SECTION] $and Operator

	// Allows us to find documents matching criteria in a single field
	/*
		db.collectionName.find({ $and: [ { fieldA: {$lt: valueA} }, { fieldB: {$gt: valueB} }] })
	*/

db.users.find({ $and: [ { age: {$lt: 70} }, { age: {$gt: 60} }] });



//  ==== [SECTION] Field Projection

	// Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response
	// When dealing with complex data structures, there might be instances when field are not useful for the query that we are trying to accomplish

// [SUB-SECTION] INCLUSION

	// Allows us to include/add specific field only when retrieving documents
	// The values provided is 1 to denote that the field is being included
	/*
		Syntax:
			db.collectionName.find({ criteria }, { field: 1 });
	*/	

db.users.find({ firstName: "Jane" }, { firstName: 1, lastName: 1, contact: 1 });

// otherway to code inclusion
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)


// [SUB-SECTION] EXCLUSION

	// Allow us to exclude/remove specific fields only when retrieving documents
	// The provided is 0 to denote that the field is being excluded
	/*
		Syntax:
			db.collectionName.find({ criteria}, { field: 0});
	*/

db.users.find({ firstName: "Jane"}, { contact: 0, department: 0 });



// [SUB-SECTION] Suppressing the ID Field

	// Allows us to exclude the "_id" field when retrieving documents
	/*
		Syntax:
			db.collectionName.find({ criteria }, { _id: 0 })
	*/

db.users.find({ firstName: "Jane"}, { firstName: 1, lastName: 1, contact: 1, _id: 0 });


// [SUB-SECTION] Returning Specific Fields in Embedded Documents

db.users.find( { firstName: "Jane" }, { firstName: 1, lastName: 1, "contact.phone": 1});


// [SUB-SECTION] Supressing Specific Fields in Embedded Documents

db.users.find( { firstName: "Jane" }, { "contact.phone": 0 } );




// ===== [SECTION] Evaluation Query Operators



// [SUB-SECTION] $regex Operators

	// Allows us to find documents that match a specific string pattern using regular expressions.
	/*
		Syntax:
			db.collectionName.find( { field: $regex : 'pattern', $options: '$optionValue'})
	*/

// Case sentitive query
db.users.find( { firstName: { $regex: 'N' } });

// Case insensitive query
db.users.find( { firstName: { $regex: 'J', $options: 'i' } });