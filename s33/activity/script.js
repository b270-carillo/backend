//3 and 4
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    console.log(data.map((item) => item.title));
  })
  .catch((error) => console.error(error));

//5
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch((error) => console.error(error));

//6
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((data) => {
    console.log(
      `The item "${data.title}" on the list has a status of ${data.completed}`
    );
  })
  .catch((error) => console.error(error));


//7
  fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Created To Do List Item",
    completed: false,
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data));

//8 and 9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Updated To Do List Item",
    description: "To update the my to do list with a different data structure",
    status: "pending",
    dataCompleted: "Pending",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// 10 and 11
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    status: "Complete",
    dataCompleted: new Date().toLocaleDateString(),
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));


